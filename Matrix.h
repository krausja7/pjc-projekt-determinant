#include <utility>
#include <iomanip>


using namespace std;

template<typename T>
class Matrix{

private:

    vector<T> data;
    size_t row;
    size_t col;

public:

    Matrix();
    Matrix(size_t row, size_t col, vector<T> data);
    Matrix(const Matrix<T>& matrix);

    Matrix<T>& operator=(const Matrix<T>& matrix);
    T& operator()(size_t i, size_t j);
    T operator()(size_t i, size_t j) const;
    bool operator==(const Matrix<T>& matrix) const;
    bool operator!=(const Matrix<T>& matrix) const;
    Matrix<T>& operator+=(const Matrix<T>& matrix);
    Matrix<T>& operator-=(const Matrix<T>& matrix);
    Matrix<T> operator+(const Matrix<T>& matrix) const;
    Matrix<T> operator-(const Matrix<T>& matrix) const;

    void setValueOnCoordinate(size_t i, size_t j, const T& value);
    size_t getNumberOfRows()const {return row;};
    size_t getNumberOfColumns()const {return col;};
    void print();

};

template<typename T>
Matrix<T>::Matrix() : data(nullptr), row(0), col(0){
}

template<typename T>
Matrix<T>::Matrix(size_t row, size_t col, vector<T> data) : data(data), row(row), col(col) {

}

template<typename T>
Matrix<T>::Matrix(const Matrix<T> &matrix) {
    this->data = matrix.data;
    this->row = matrix.row;
    this->col = matrix.col;
}

template<typename T>
Matrix<T> &Matrix<T>::operator=(const Matrix<T> &matrix) {
    *this->data = matrix;
    return *this;
}

template<typename T>
T &Matrix<T>::operator()(size_t i, size_t j) {
    if((i > row) or (j > col)){
        throw std::out_of_range("Out of the range of matrix!");
    }
    return this->data[i * col + j];
}

template<typename T>
T Matrix<T>::operator()(size_t i, size_t j) const {
    if((i > row) or (j > col)){
        throw std::out_of_range("Out of the range of matrix!");
    }
    return this->data[i * col + j];
}

template<typename T>
bool Matrix<T>::operator==(const Matrix<T> &matrix) const {
    return this->data == matrix.data;
}

template<typename T>
bool Matrix<T>::operator!=(const Matrix<T> &matrix) const {
    return this->data != matrix.data;
}

template<typename T>
Matrix<T> &Matrix<T>::operator+=(const Matrix<T> &matrix) {
    if((row != matrix.row) and (col != matrix.col)){
        throw std::out_of_range("Out of the range of matrix!");
    }
    this->data += matrix.data;
    return *this;
}

template<typename T>
Matrix<T> &Matrix<T>::operator-=(const Matrix<T> &matrix) {
    if((row != matrix.row) and (col != matrix.col)){
        throw std::out_of_range("Out of the range of matrix!");
    }
    this->data -= matrix.data;
    return *this;
}

template<typename T>
Matrix<T> Matrix<T>::operator+(const Matrix<T> &matrix) const {
    if((row != matrix.row) and (col != matrix.col)){
        throw std::out_of_range("Out of the range of matrix!");
    }
    this->data += matrix.data;
    return *this;
}

template<typename T>
Matrix<T> Matrix<T>::operator-(const Matrix<T> &matrix) const {
    if((row != matrix.row) and (col != matrix.col)){
        throw std::out_of_range("Out of the range of matrix!");
    }
    this->data -= matrix.data;
    return *this;
}

template<typename T>
void Matrix<T>::print() {
    cout << setprecision(2);
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
            cout << setw(10) << data[i * col + j] << setw(10);
        }
        cout<<endl;
    }
    cout << endl;
}

template<typename T>
void Matrix<T>::setValueOnCoordinate(size_t i, size_t j, const T &value) {
    this->data[i * col + j] = value;
}





