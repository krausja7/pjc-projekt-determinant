#include "Matrix.h"
#include "Helper.h"

Matrix<double> lowerUpperDecomposition(Matrix<double> matrix){
    size_t n = matrix.getNumberOfRows();
    Matrix<double> lower(n, n, getZeroVectorOfSize(n*n));
    Matrix<double> upper(n, n, getZeroVectorOfSize(n*n));

    for (size_t i = 0; i < n; i++) {
        for (size_t k = i; k < n; k++) {
            double sum = 0;
            for (size_t j = 0; j < i; j++){
                sum += (lower(i, j) * upper(j, k));
            }
            upper.setValueOnCoordinate(i, k, matrix(i, k) - sum);
        }

        for (size_t k = i; k < n; k++) {
            if (i == k){
                lower.setValueOnCoordinate(i, i, 1);
            } else {
                double sum = 0;
                for (size_t j = 0; j < i; j++){
                    sum += (lower(k, j) * upper(j, i));
                }
                lower.setValueOnCoordinate(k, i, (matrix(k, i) - sum) / upper(i, i));
            }
        }
    }
    return upper;
}

long double getDeterminantOfUpperMatrix(Matrix<double> matrix){
    long double determinant = 1;
    size_t n = matrix.getNumberOfRows();
    for (size_t i = 0; i < n; ++i) {
        determinant *= matrix(i, i);
    }
    return determinant;
}