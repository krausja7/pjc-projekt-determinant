# Programování v jazyce C/C++
# Výpočet determinantu

Determinant je počítán algoritmem LU dekompozice, kdy matici rozložíme jako součin
dvou dalších matic, z nichž jedna (L z anglického lower) je v dolním trojúhelníkovém
tvaru se samými jedničkami na diagonále, a druhá (U z anglického upper) je v horním 
trojúhelníkovém tvaru, která má na diagonále samé nenulové prvky.

Program pro výpočet využívá implementovaný náhodný generátor matic.

Program přijímá na vstupu tři argumenty:

 - velikost matice
 - počet matic
 - použití multithreadingu (0 - single thread, 1 - multiple thread)

Při rozhodování, jaký algoritmus použít, jsem se chtěl rozhodovat podle toho, 
který algoritmus půjde nejlépe paralelizovat. U všech mnou nalezených algoritmů
jsem však narazil na problém, kdy každý výpočet záleží na výsledku předchozího,
což není ideální problém pro paralelizaci. 

Kvůli výše uvedeným problémům jsem se rozhodl paralelizaci provádět při výpočtu 
několika matic, abych splnit podmínku paralelizace programu.

Vytvořil jsem si vlastní třídu matice, kdy jsou data ukládána do 1D vektoru,
což je na základě mého testování rychlejší než double array, či vektor vektorů.

# Srovnání času single thread vs. multiple thread

Pět matic o velikosti 1000x1000
 - single thread (21148 ms)
 - multiple thread (10578 ms)

Deset matic velikosti 500x500
 - single thread (5288 ms)
 - multiple thread (2850 ms)

Padesát matic velikosti 100x100
 - single thread (220 ms)
 - multiple thread (98 ms)
 
