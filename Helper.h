#include <random>
#include <chrono>

using namespace std;

enum argument{ HELP, SINGLE, MULTI, INVALID};

int getRandomInt(int min, int max) {
    random_device rd;
    mt19937 mt(rd());
    uniform_int_distribution<int> dist(min, max);
    return dist(mt);
}

vector<double> getRandomVectorOfSize(size_t size){
    vector<double> data;
    for (int i = 0; i < size; ++i) {
        data.push_back(getRandomInt(-10, 10));
    }
    return data;
}

vector<double> getZeroVectorOfSize(size_t size){
    vector<double> data;
    for (int i = 0; i < size; ++i) {
        data.push_back(0);
    }
    return data;
}

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

int str_to_int(std::string const &str) {
    std::size_t pos;
    int res = std::stoi(str, &pos);
    if (pos != str.size())
        throw std::invalid_argument("Numeric string is invalid!");
    return res;
}

argument parseArguments(int &num_of_args, char* arguments[]){
    if (num_of_args == 2 && strcmp(arguments[1], "--help") == 0) {
        return HELP;
    } else if (num_of_args == 4 && strcmp(arguments[3], "0") == 0) {
        str_to_int(arguments[2]);
        str_to_int(arguments[3]);
        return SINGLE;
    } else if (num_of_args == 4 && strcmp(arguments[3], "1") == 0) {
        str_to_int(arguments[2]);
        str_to_int(arguments[3]);
        return MULTI;
    } else {
        return INVALID;
    }
}

