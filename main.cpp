#include "main.h"

int main(int argc, char *argv[]) {

    argument a = parseArguments(argc, argv);

    switch (a) {
        case HELP: {
            std::cout << "Run with these three arguments to generate random matrices.\n"
                         "First argument: size of matrices\n"
                         "Second argument: number of matrices to be determined\n"
                         "Third argument: '0' for single-threaded calculation / '1' for multi-threaded calculation\n";
            break;
        }
        case SINGLE: {
            int numberOfMatrices = std::stoi(argv[2]);
            size_t n = static_cast<size_t>(std::stoi(argv[1]));

            vector<Matrix<double>> matrices;
            matrices.reserve(static_cast<unsigned long>(numberOfMatrices));
            for (int i = 0; i < numberOfMatrices; ++i) {
                matrices.emplace_back(n, n, getRandomVectorOfSize(n*n));
            }

            auto start = chrono::high_resolution_clock::now();

            for (Matrix<double>& matrix : matrices) {
                Matrix<double> upper = lowerUpperDecomposition(matrix);
                long double determinant = getDeterminantOfUpperMatrix(upper);
                cout << "Determinant is: " << determinant << "\n";
            }

            auto end = chrono::high_resolution_clock::now();
            std::cout << "Needed " << to_ms(end - start).count() << " ms to finish.\n";

            break;
        }
        case MULTI: {
            int numberOfMatrices = std::stoi(argv[2]);
            size_t n = static_cast<size_t>(std::stoi(argv[1]));
            vector<future<long double>> futures;
            futures.reserve(static_cast<unsigned long>(numberOfMatrices));

            vector<Matrix<double>> matrices;
            matrices.reserve(static_cast<unsigned long>(numberOfMatrices));
            for (int i = 0; i < numberOfMatrices; ++i) {
                matrices.emplace_back(n, n, getRandomVectorOfSize(n*n));
            }

            auto start = std::chrono::high_resolution_clock::now();
            for (Matrix<double>& m : matrices) {
                futures.emplace_back(std::async(std::launch::async, [&m]() {
                    Matrix<double> upper = lowerUpperDecomposition(m);
                    long double determinant = getDeterminantOfUpperMatrix(upper);
                    return determinant;
                }));
            }
            for (auto & f : futures) {
                f.wait();
                std::cout << "Determinant is: " << f.get() << std::endl;
            }

            auto end = std::chrono::high_resolution_clock::now();
            std::cout << "Needed " << to_ms(end - start).count() << " ms to calculate the determinants.\n";

            break;
        }
        case INVALID: {
            std::cout << "Invalid arguments!";
            break;
        }
    }

}